# Test Data Management

Test driven development requires a decision to be made on how to manage test dependent data.

## Requirements

1. Can be used for managing test data for the entire platform
   - Common approach for all test scenarios
      - Unit testing of all application layers
      - Integration testing
      - QA environments
1. Configurable
   - Declarative
   - High level abstraction
   - Specific when necessary
1. Business rules are enforced
   - Data can be trusted
1. Tracked data
   - Support for parallel testing
1. Transactional or non transactional
   - Support for tests where database rollback may not an option

## History

1. Using raw database fixtures
   - Very complex create
   - Lots of files
   - Very difficult to maintain
   - Skips business rules
   - Cannot trust data
   - Cannot run parallel tests
1. Using the data layer directly
   - Complex to create
   - Lots of boilerplate
   - Difficult to maintain
   - Skips business rules
   - Cannot trust data

## Implementation

To implement out test data management approach our application architecture has a few requirements:

1. We have a separation of our application (or use case) layer from our model layer.
1. All model layer methods are exposed for use by the application layer.
1. *Optional* - The data layer can be mocked.

### Layered Architecture Overview

- Application layer
  - Example Responsibilities:
    - Input JSON payload validates against JSON schema.
    - Client accessing API is authenticated and has appropriate roles.
    - Fetch all unapplied customer payments and attempt to apply each one.
    - Fetch all event messages that haven't been sent and send them.
- Model layer
  - Example Responsibilities:
    - The specified customer must be active for a charge to be created.
    - A charge cannot already be paid to apply a payment to it.
    - A seller can only have one tax identifier for GST.
    - A buyer can only have currencies allowed by the program.
    - A customer must have VAT tax configuration if their location is in Europe before they can be activated.
- Data layer
  - Example Responsibilities:
    - CRUD
- Storage

```mermaid
graph LR;
  subgraph layer1 ["Application Layer (Use Case)"]
    app1["API - Create charge"]
    app2["Processing - Generate invoice"]
    app3["Testing -> Create test data"]
  end
  subgraph layer2 ["Model Layer (Business Rules) ①"]
    model1["Charge"]
    model2["Invoice"]
    model3["Customer"]
    model4["Payment"]
  end
  subgraph layer3 ["Data Layer (CRUD) ②"]
    data1["Charge"]
    data2["Charge Activity"]
    data3["Invoice"]
    data4["Customer"]
    data5["Payment"]
    data6["Buyer"]
  end
  subgraph layer4 ["Storage ③"]
    store1["Postgres DB"]
  end
  %% Application layer to model layer
  app1 --> model1
  app2 --> model1
  app2 --> model2
  app3 --> model1
  app3 --> model2
  app3 --> model3
  %% Model layer to data layer
  model1 --> data1
  model1 --> data2
  model1 --> data6
  model2 --> data1
  model2 --> data3
  model2 --> data4
  model3 --> data4
  model4 --> data5
  %% Data layer to storage layer
  data1 --> store1
  data2 --> store1
  data3 --> store1
  data4 --> store1
  data5 --> store1
  data6 --> store1
```

### On the topic of mocking

Hopefully a quick side step..

#### ① - Mock the model layer

❌ Each model is a snowflake

❌ Can describe an interface per model but it will change often

❌ Requires greater domain expertise

❌ Risk skipping critical business rules when you mock any business rule layer function

#### ② - Mock the data layer

✅ Every data repository is generic

- `getOne(id) (record, err)`
- `getMany(params) ([]records, err)`
- `insertOne(record) (record, err)`
- `updateOne(record) (record, err)`
- `deleteOne(id) (err)`
- Etc..

✅ Can describe an interface per repository and won't change often (if at all..)

✅ Requires little to no domain expertise

✅ Does not skip business rules as there are none at this layer!

❌ Complex to implement a generic mocked function for `getMany(params) ([]records, err)`

#### ③ - Mock the stor... blahrghh.. Forget about it

❌ Implemented with third-party packages / database drivers

❌ More than likely no interface to work with

[Detailed layered architecture explanation](ArchitectureLayered.md)

## Implementation Examples

The following examples are taken from the "replica" CaaS API built to support the Staples mobile application development.

### Configuration

The test data manager provides configuration using Go structures:

```golang
// Testing -
type Testing struct {
  harness.Testing
  Data       *Data
  DataConfig DataConfig
}

// DataConfig -
type DataConfig struct {
  // ProgramConfig -
  ProgramConfig []ProgramConfig
}

// ProgramConfig -
type ProgramConfig struct {
  Record record.Program
  // CustomerConfig -
  CustomerConfig []CustomerConfig
  // SellerConfig -
  SellerConfig []SellerConfig
  // AuthorisationConfig -
  AuthorisationConfig []AuthorisationConfig
  // KeyConfig -
  KeyConfig []KeyConfig
}
...
// CustomerConfig -
type CustomerConfig struct {
  Record record.Customer
  // BuyerConfig -
  BuyerConfig []BuyerConfig
  // PaymentConfig -
  PaymentConfig []PaymentConfig
}

// BuyerConfig -
type BuyerConfig struct {
  Record record.Buyer
  // BuyerCodeConfig -
  BuyerCodeConfig []BuyerCodeConfig
  // AccountConfig -
  AccountConfig []AccountConfig
}
...
// AuthorisationConfig -
type AuthorisationConfig struct {
  Record       record.Authorisation
  BuyerIdx     int
  SellerIdx    int
  Count        int
  Periodic     time.Duration
  ChargeConfig []ChargeConfig
}

// ChargeConfig -
type ChargeConfig struct {
  Record             record.Charge
  ChargeDetailConfig []ChargeDetailConfig
  Count              int
}
...
```

- Structured to align with the application data relationships
- Provides higher level abstractions
- Provides specific definition when required

🔗 **Links**

- [Complete configuration](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/testing.go#L16)
- [Setup data](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/testing.go#L166)
- [Customer data creation](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/customer.go)
- [Charge data creation](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/charge.go)
- [Teardown data](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/testing.go#L592)

### API Testing

A typical API unit test setup might look like this:

```golang
func TestAuthHandler(t *testing.T) {

  // Instantiate test harness
  th, err := NewTestHarness()
  require.NoError(t, err, "New test data returns without error")
  ...
  // For each test
  {
    // Setup test data
    err = th.Setup()
    require.NoError(t, err, "Test data setup returns without error")
    defer func() {
      // Teardown test data
      err = th.Teardown()
      require.NoError(t, err, "Test data teardown returns without error")
    }()
    // Run test
    ...
  }
}
```

The above example API test uses a shared test harness creation method that provides a standard test data configuration.

As the test data configuration is passed into the test harness instantiation function it is simple enough to implement specific test data configurations when necessary.

```golang
func NewTestHarness() (*harness.Testing, error) {

  // Standard test data configuration
  config := harness.StandardConfig

  h, err := harness.NewTesting(config)
  if err != nil {
    return nil, err
  }

  // Commit data for API tests
  h.CommitData = true

  return h, nil
}
```

🔗 **Links**

- [Test data manage instantiation](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/harness/testing.go#L130)

### QA Environment

When the application is deployed to the QA environment the same test data management mechanism is used to populate the freshly created database with test data.

- Dockerfile [entrypoint](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/entrypoint.sh#L53) calls a [CLI](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/cli/runner/runner.go#L19) to `load-test-data`
- CLI [populates](https://gitlab.com/msts-enterprise/caas/trevipay-mobile/-/blob/develop/server/service/api/internal/cli/runner/testdata.go#L19) database
