# Notes

Notes on all the things..

## Index

### Architecture

- [Layered Architecture](ArchitectureLayered.md)
- [Test Data Management](ArchitectureTestDataManagement.md)

### CaaS

- [Creating a new cronjob from the ground up](CaaSCreateCronjobGroundUp.md)
