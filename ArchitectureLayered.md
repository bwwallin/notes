# Layered Architecture

A basic layered architecture approach.

1. Application Layer
1. Model Layer
1. Data Layer
1. Storage Layer

## Layered Architecture Graph

```mermaid
graph LR;
    subgraph lapp [Application Layer /cmd]
    app1["API - postCharge()"]
    app2["Job - processPayments()"]
    end
    subgraph lmodel [Model Layer /model]
      subgraph mcharge [Charge Model]
        mcharge1["getCharge(id)"]
        mcharge2["getCharges(params)"]
        mcharge3["createCharge(rec)"]
        mcharge4["updateCharge(rec)"]
        mcharge5["deleteCharge(id)"]
      end
      subgraph mpayment [Payment Model]
        mpayment1["getPayments(params)"]
        mpayment4["updatePayment(rec)"]
      end
    end
    subgraph data ["Data Layer /repository"]
      subgraph dbuyer [Buyer Repository]
        dbuyer1["getOne(id)"]
        dbuyer2["getMany(params)"]
        dbuyer3["createOne(rec)"]
        dbuyer4["updateOne(rec)"]
        dbuyer5["deleteOne(id)"]
      end
      subgraph dcharge [Charge Repository]
        dcharge1["getOne(id)"]
        dcharge2["getMany(params)"]
        dcharge3["createOne(rec)"]
        dcharge4["updateOne(rec)"]
        dcharge5["deleteOne(id)"]
      end
      subgraph dpayment [Payment Repository]
        dpayment1["getOne(id)"]
        dpayment2["getMany(params)"]
        dpayment3["createOne(rec)"]
        dpayment4["updateOne(rec)"]
        dpayment5["deleteOne(id)"]
      end
    end
    subgraph storage ["Storage"]
        database1["Postgres Database"]
    end

    %% Data layer to storage layer
    dbuyer1-->database1
    dbuyer2-->database1
    dbuyer3-->database1
    dbuyer4-->database1
    dbuyer5-->database1

    dcharge1-->database1
    dcharge2-->database1
    dcharge3-->database1
    dcharge4-->database1
    dcharge5-->database1

    dpayment1-->database1
    dpayment2-->database1
    dpayment3-->database1
    dpayment4-->database1
    dpayment5-->database1

    %% Create a charge workflow
    app1-->mcharge3
    mcharge3-->dbuyer1
    mcharge3-->dcharge3
    %% Process payments workflow
    app2-->mpayment1
    mpayment1-->dpayment2
    app2-->mcharge2
    mcharge2-->dcharge2
    mcharge4-->dcharge4
    app2-->mpayment4
    mpayment4-->dpayment4
    app2-->mcharge4
```

## Application Layer Example

Send A POST charge request to the REST API `/api/charges` endpoint.

```mermaid
graph TD;
    subgraph app1 ["Application Layer / API - postCharge()"]
        subgraph midd1 ["Common middleware"]
            step1["Authenticate request (Key, JWT)"]
            step2["Read request data"]
            step3["Validate request data (Enforce JSON schema)"]
        end
        step4["Transform request data to record"]
        step5["--> Model - createCharge(rec)"]
        step6["Transform record to response data"]
        step7["Write success response"]
        step8["Write failure response"]
        step1-->step2
        step1-->step8
        step2-->step3
        step3-->step4
        step3-->step8
        step4-->step5
        step5-->step6
        step5-->step8
        step6-->step7
    end
```

- The application layer defines the interface for the model layer:
  - I need a "charge" model that looks like ...
- Common middleware implements:
  - Authenticating the request
  - Reading request data
  - Validating request data

## Model Layer Example

Create a charge through a charge model implementation.

```mermaid
graph TD;
    subgraph app1 ["Model Layer / Charge Model - createCharge(rec)"]
        subgraph val1 ["Validation"]
        step1["Validate buyer --> Data / Buyer - getOne(id)"]
        step2["Validate charge record (Enforce business rules)"]
        end
        step3["--> Data - Charge - createOne(rec)"]
        step4["Return record"]
        step5["Return error"]
        step1-->step2
        step1-->step5
        step2-->step3
        step2-->step5
        step3-->step4
        step3-->step5
    end
```

- A model implements an interface defined by the application layer.
- The model layer defines the interface for the data layer:
  - I need a "client" repository that looks like ...
  - And I need a "buyer" repository that looks like ...
  - And I need a "charge" repository that looks like ...
- Provide the ability to instantiate an implementation of a model with the ability to override or replace any model method for exercising specific unit test cases.
  - Saves engineering time as an engineer doesn't need to implement their own implementation every time they want to mock a models methods.
  - Typically error unit test cases.

The model layer contains business rules and complex logic so is not practical for the model layer to provide a generic mock implementation.

## Data Layer Example

Create a charge record through a charge repository implementation.

```mermaid
graph TD;
    subgraph app1 ["Data Layer / Charge Repository - createOne(rec)"]
        step1["--> Storage - 'INSERT INTO ...'"]
        step2["Return record"]
        step3["Return error"]
        step1-->step2
        step1-->step3
    end
```

- Entire data layer could be an ORM
- Otherwise, individual repositories provide a standard set of methods:
  - `getOne(id)`
  - `getMany(params)`
  - `createOne(rec)`
  - `createMany(recs)`
  - `updateOne(rec)`
  - `updateMany(recs)`
  - `deleteOne(id)`
- A repository implements an interface defined by the model layer.
- Provide the ability to instantiate an implementation of a repository with the ability to override or replace any repository method for exercising specific unit test cases.
  - Saves engineering time as an engineer doesn't need to implement their own implementation every time they want to mock a repositories methods.
  - Typically error unit test cases.

### Optional Generic Repository Mock

The repository layer does not contain business rules or complex logic so it is practical to implement a generic mock that emulates the storage layer.

- Provide standard set of methods
- Provide the ability to instantiate a generic mock implementation of a repository with the ability to override or replace any mocked repository method to exercise specific unit test cases.
  - Typically error cases
  - Override `createOne(rec)` method so it always returns an error
- Store data in key/value structures
- No foreign key integrity

## Storage Layer

- Implemented by third party drivers
- No interface definitions to implement
- No value in mocking
