# Creating a CaaS cronjob from the ground up.

A summary of what is required to create a new cronjob in the CaaS project. 

The specific case scenario is creating a new cronjob to create a billing event message.

1) Create a service that a cronjob would call on

A service contains a mixture of process logic and business rules.

`./src/event-message/services/index.ts`

- Contains `createInstance` factory method, returning interface type `EventMessageService`

`./src/event-message/services/index.types.ts`

- Contains interface `EventMessageService` definition

2) Create a model that a "service" would call on.

A model in CaaS appears to essentially be the data layer.

`./src/event-message/services/model.ts`

2) Create cronjob

`./src/cronjobs/process-create-billing-event-messages.ts`

- Noticed a mix of `main()` versus `runJob()` implementations




